#!/bin/sh
### BEGIN INIT INFO
# Required-Start: hmi_demo
# Should-Start: 
# Should-Stop: weston
# Default-Start: 
# Default-Stop: 
# Short-Description: 
# Description: 
### END INIT INFO

start() {
    # stop weston
    # systemctl stop weston

    # setup XDG_RUNTIME_DIR if not already done
    if test -z "$XDG_RUNTIME_DIR"; then
        export XDG_RUNTIME_DIR=/tmp/`id -u`-runtime-dir
        if ! test -d "$XDG_RUNTIME_DIR"; then
            mkdir --parents $XDG_RUNTIME_DIR
            chmod 0700 $XDG_RUNTIME_DIR
        fi
    fi
    
    # This step will to ensure someone has clicked the Camera Demo Icon and just stopping weston won't start Camera Demo 
    FILE="/tmp/camera.txt"
    if [ -f "$FILE" ]; then
        if grep -q "1" "$FILE"; then
  	    rm $FILE
	fi
    else
        exit
    fi

    # wait if weston is still up
    while [ -e  $XDG_RUNTIME_DIR/wayland-0 ] ; do sleep 0.1; done

    DEMO_Camera_CMD=`timeout 30s gst-launch-1.0 v4l2src device="/dev/video0" ! video/x-raw, width=640, height=480, format=UYVY ! kmssink driver-name=tidss`

    # Launch Camera Demo
    eval $DEMO_Camera_CMD
    
    rm /var/run/hmi_demo.pid 

    systemctl start weston
    systemctl start hmi_demo

    exit
}

case "$1" in
    start )
        start
    ;;
    stop )
        echo "Stop not implemented because this is a one time script."
    ;;
    restart )
        stop
        start
    ;;
    * )
        echo "Usage: $0 {start|stop|restart}"
esac
